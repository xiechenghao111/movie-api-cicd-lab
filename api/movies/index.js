import express from 'express';
import { movies, movieReviews, movieDetails } from './moviesData';
import uniqid from 'uniqid'
import movieModel from './movieModel';
import asyncHandler from 'express-async-handler';
import { getUpcomingMovies } from '../tmdb-api';
import { getTopratedMovies } from '../tmdb-api';
import { getNowPlayingMovies } from '../tmdb-api';
import { getMovieImages } from '../tmdb-api';
import { getMovies } from '../tmdb-api';
import { getMovieDetail } from '../tmdb-api';
import { getMovieReviews } from '../tmdb-api';
const router = express.Router(); 
router.get('/', asyncHandler(async (req, res) => {
    const movies = await movieModel.find();
    res.status(200).json(movies);
}));

router.get('/tmdb/popular', asyncHandler( async(req, res) => {
    const Movies = await getMovies();
    res.status(200).json(Movies);
  }));

  router.get('/tmdb/movies/:id', asyncHandler( async(req, res) => {
   
    const MovieDetail = await getMovieDetail(req.params.id);
    res.status(200).json(MovieDetail);
  }));

   
  
  router.get('/tmdb/movies/:id/images', asyncHandler( async(req, res,) => {
    const MovieImages = await getMovieImages(req.params.id);

      res.status(200).json(MovieImages);

}));
    

  router.get('/tmdb/movies/:id/reviews', asyncHandler( async(req, res,) => {
    const MovieReviews = await getMovieReviews(req.params.id);
    res.status(200).json(MovieReviews);
  }));

router.get('/tmdb/upcoming', asyncHandler( async(req, res) => {
    const upcomingMovies = await getUpcomingMovies();
    res.status(200).json(upcomingMovies);
  }));

  router.get('/tmdb/toprated', asyncHandler( async(req, res) => {
    const topratedMovies = await getTopratedMovies();
    res.status(200).json(topratedMovies);
  }));

  router.get('/tmdb/nowplaying', asyncHandler( async(req, res) => {
    const nowplayingMovies = await getNowPlayingMovies();
    res.status(200).json(nowplayingMovies);
  }));
  

 
  
export default router;


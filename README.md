# Assignment 2 - Agile Software Practice.
​
Name: chenghao

## API endpoints.
​
+ GET api/movies/tmdb/popular - Get 20 popular movies.
+ GET api/movies/tmdb/movies/:id - get the details of the movies.
+ GET api/movies/tmdb/movies/:id/images - get the images of the movies.
+ GET api/movies/tmdb/nowplaying - get the 20 nowplaying movies.
+ GET api/movies/tmdb/toprated - get the 20 toprated movies.
+ GET api/users/:userName/favorites - get the movie's id in favorites of one user
+ POST api/users/userName/favorites - add a movie id to the favorites of one user
+ GET api/genres - get 4 genres of the movies
+ POST api/genres - add one genre of the movie
+ PUT api/genres - change one genre of the movie
+ GET api/people/tmdb/popular - get 20 poopular people
+ GET api/people/tmdb/people/:id - get the details of the people
+ GET api/people/tmdb/people/:id/images - get the images of the people
+ GET api/people/tmdb/:id/movie-credits - get  the movie-credits of the people
+ GET api/TV/tmdb/popular - get the 20 popular TVs
+ GET api/TV/tmdb/TV/:id - get the details of the TV
+ GET api/TV/tmdb/TV/:id/similar - get the similar TV images of the TV
+ GET api/TV/tmdb/TV/:id/reviews - get the TV reviews of the TV
+ GET api/TV/tmdb/TV/:id/genres - get the genres of the TV

## Test cases.
~~~
 Users endpoint
    GET /api/users/:userName/favorites
      √ should return user's favorites movies and status 200 (586ms)
    POST /api/users
      For a register action
        when the payload is correct
         √ should return a 201 status and the confirmation message (157ms)
        when the payload is wrong
          √ should return a 401 status and the remind message (157ms)
      For an authenticate action
        when the payload is correct
          √ should return a 200 status and a generated token (180ms)
      when the password is incorrect
        √ should return a 401 status and an error message (39ms)
      when the username and passname is no pass
        √ should return a 401 status and the remind message

  POST /api/users
    when the payload is correct
      √ should return a 201 status and the confirmation message (159ms)

  POST /api/users/:userName/favorites
    √ should add a movie id to user1's favorites  (55ms)

  Movies endpoint
    GET /api/movies/tmdb/upcoming
      √ should return 20 upcomingmovies and a status 200 (237ms)
    GET /api/movies/tmdb/toprated
      √ should return 20 topratedmovies and a status 200 (71ms)
    GET /api/movies/tmdb/nowplaying
      √ should return 20 nowplayingmovies and a status 200 (100ms)
    GET /api/movies/tmdb/popular
      √ should return 20 popularmovies and a status 200 (76ms)
    GET /api/movies/tmdb/movies/:id
      √ should return the detail of one movie (90ms)
      GET /api/movies/tmdb/movies/:id/reviews
        √ should return the reviews of one movie (85ms)

  People endpoint
    GET /api/people/tmdb/popular
      √ should return 20 people who are popular and a status 200 (68ms)
    GET /api/people/tmdb/people/:id
      √ should return the detail of one people (825ms)
    GET /api/people/tmdb/people/:id/images
      √ should return the people's images (63ms)
    GET /api/people/tmdb/:id/movie-credits
      √ should return the people's movie-credits (105ms)

  TV endpoint
    GET /api/TV/tmdb/popular
      √ should return 20 TVs and a status 200 (71ms)
    GET /api/TV/tmdb/TV/:id
      √ should return teh details of a TV (67ms)
    GET /api/TV/tmdb/TV/:id
      √ should return the details of a TV (106ms)
    GET /api/TV/tmdb/TV/:id/similar
      √ should return the similar images of a TV (71ms)
    GET /api/TV/tmdb/TV/:id/reviews
      √ should return the reviews of a TV (81ms)
    GET /api/TV/tmdb/TV/:id/genres
      √ should return the genre of a TV (71ms)

  Genres endpoint
    GET /api/genres
      √ should return 4 genres and a status 200 (46ms)
     GET /api/genres/tmdb/movies
      √ should return 4 genres and a status 200


  26 passing (11s)
~~~

## Independent Learning 

Use the Coveralls webpage that contains my tests' code coverage metrics. This is the 
url of the webpage https://coveralls.io/gitlab/xiechenghao111/movie-api-cicd-lab. And add to the gitlab.



 

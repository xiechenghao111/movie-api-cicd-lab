import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import User from "../../../../api/users/userModel";
import api from "../../../../index";

const expect = chai.expect;
let db;
let user1token;

describe("Users endpoint", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });
  beforeEach(async () => {
    try {
      await User.deleteMany();
      // Register two users
      await request(api).post("/api/users?action=register").send({
        username: "user1",
        password: "test1",
      });
      await request(api).post("/api/users?action=register").send({
        username: "user2",
        password: "test2",
      });
    } catch (err) {
      console.error(`failed to Load user test Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close();
  });

  describe("GET /api/users/:userName/favorites", () => {
    it("should return user's favorites movies and status 200", (done) => {
      request(api)
      .get("/api/users/user1/favorites")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .end((err, res) => {
        expect(res.body).to.be.a("array");
        done();
      });
    });
  });

  describe("POST /api/users ", () => {
   
    describe("For a register action", () => {
      describe("when the payload is correct", () => {
        it("should return a 201 status and the confirmation message", () => {
          return request(api)
            .post("/api/users?action=register")
            .send({
              username: "user3",
              password: "test3",
            })
            .expect(201)
            .expect({ msg: "Successful created new user.", code: 201 });
        });
        after(() => {
          return request(api)
            .get("/api/users")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((res) => {
              expect(res.body.length).to.equal(3);
              const result = res.body.map((user) => user.username);
              expect(result).to.have.members(["user1", "user2", "user3"]);
            });
        });
      });
      describe("when the payload is wrong", () => {
        it("should return a 401 status and the remind message", () => {
          return request(api)
            .post("/api/users?action=register")
            .send({
              username: "user3",
              password: "te1",
            })
            .expect(401)
            .expect({ msg: "Authentication failed. Wrong password.", code: 401 });
        });
      });
    });
    describe("For an authenticate action", () => {
      describe("when the payload is correct", () => {
        it("should return a 200 status and a generated token", () => {
          return request(api)
            .post("/api/users?action=authenticate")
            .send({
              username: "user1",
              password: "test1",
            })
            .expect(200)
            .then((res) => {
              expect(res.body.success).to.be.true;
              expect(res.body.token).to.not.be.undefined;
              user1token = res.body.token.substring(7);
            });
        });
      });
    });
    describe("when the password is incorrect", () => {
      it("should return a 401 status and an error message", () => {
        return request(api)
          .post("/api/users?action=authenticate")
          .send({
            username: "user4",
            password: "test4",
          })
          .expect(401)
          .expect({code: 401,msg: 'Authentication failed. User not found.'})
      });
    });
    describe("when the username and passname is no pass", () => {
      it("should return a 401 status and the remind message", () => {
        return request(api)
          .post("/api/users?action=authenticate")
          .send({
            falseInfo: "falseInfo"
          })
          .expect(401)
          .expect({ success: false, msg: "Please pass username and password."});
      });
    });
  });
  
});
describe("POST /api/users ", () => {
  describe("when the payload is correct", () => {
    it("should return a 201 status and the confirmation message", () => {
      return request(api)
        .post("/api/users?action=register")
        .send({
          username: "user3",
          password: "test3",
        })
        .expect(201)
        .expect({ msg: "Successful created new user.", code: 201 });
    });
  });
});

describe("POST /api/users/:userName/favorites", () => {
  it("should add a movie id to user1's favorites ", () => {
    return request(api)
    .post("/api/users/user1/favorites")
    .send({newFavorite: 64690})
  
  });
});

  
  
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));
var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));
var _express = _interopRequireDefault(require("express"));
var _expressAsyncHandler = _interopRequireDefault(require("express-async-handler"));
var _reviewModel = _interopRequireDefault(require("./reviewModel"));
var router = _express["default"].Router();
// Get movie reviews
router.get('/', /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
    var review;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return _reviewModel["default"].find();
        case 2:
          review = _context.sent;
          res.status(200).json(review);
        case 4:
        case "end":
          return _context.stop();
      }
    }, _callee);
  }));
  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}());
router.post('/', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
    var review;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) switch (_context2.prev = _context2.next) {
        case 0:
          if (!(req.query.action === 'register')) {
            _context2.next = 6;
            break;
          }
          _context2.next = 3;
          return (0, _reviewModel["default"])(req.body).save();
        case 3:
          res.status(201).json({
            code: 201,
            msg: 'Successful created new reviews.'
          });
          _context2.next = 14;
          break;
        case 6:
          _context2.next = 8;
          return _reviewModel["default"].findOne(req.body);
        case 8:
          review = _context2.sent;
          if (review) {
            _context2.next = 13;
            break;
          }
          return _context2.abrupt("return", res.status(401).json({
            code: 401,
            msg: 'Authentication failed'
          }));
        case 13:
          return _context2.abrupt("return", res.status(200).json({
            code: 200,
            msg: "Authentication Successful",
            token: 'TEMPORARY_TOKEN'
          }));
        case 14:
        case "end":
          return _context2.stop();
      }
    }, _callee2);
  }));
  return function (_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}()));
router.put('/:id', /*#__PURE__*/function () {
  var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
    var result;
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) switch (_context3.prev = _context3.next) {
        case 0:
          if (req.body._id) delete req.body._id;
          _context3.next = 3;
          return _reviewModel["default"].updateOne({
            _id: req.params.id
          }, req.body);
        case 3:
          result = _context3.sent;
          if (result.matchedCount) {
            res.status(200).json({
              code: 200,
              msg: 'Review Updated Sucessfully'
            });
          } else {
            res.status(404).json({
              code: 404,
              msg: 'Unable to Update Review'
            });
          }
        case 5:
        case "end":
          return _context3.stop();
      }
    }, _callee3);
  }));
  return function (_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
}());
var _default = router;
exports["default"] = _default;
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _mongoose = _interopRequireDefault(require("mongoose"));
var Schema = _mongoose["default"].Schema;
var TVSchema = new Schema({
  backdrop_path: {
    type: String
  },
  id: {
    type: Number,
    required: true,
    unique: true
  },
  genres: [{
    name: {
      type: String
    }
  }],
  in_production: {
    type: Boolean
  },
  name: {
    type: String
  },
  original_language: {
    type: String
  },
  original_name: {
    type: String
  },
  overview: {
    type: String
  },
  popularity: {
    type: Number
  },
  poster_path: {
    type: String
  },
  tagline: {
    type: String
  },
  vote_average: {
    type: Number
  }
});
TVSchema.statics.findByPeopleDBId = function (id) {
  return this.findOne({
    id: id
  });
};
var _default = _mongoose["default"].model('TV', TVSchema);
exports["default"] = _default;
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));
var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));
var _express = _interopRequireDefault(require("express"));
var _genresModel = _interopRequireDefault(require("./genresModel"));
var _expressAsyncHandler = _interopRequireDefault(require("express-async-handler"));
var _tmdbApi = require("../tmdb-api");
var router = _express["default"].Router(); // eslint-disable-line

// Get all users
router.get('/', /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
    var genres;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return _genresModel["default"].find();
        case 2:
          genres = _context.sent;
          res.status(200).json(genres);
        case 4:
        case "end":
          return _context.stop();
      }
    }, _callee);
  }));
  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}());
router.post('/', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
    var genres;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) switch (_context2.prev = _context2.next) {
        case 0:
          if (!(req.query.action === 'register')) {
            _context2.next = 6;
            break;
          }
          _context2.next = 3;
          return (0, _genresModel["default"])(req.body).save();
        case 3:
          res.status(201).json({
            code: 201,
            msg: 'Successful created new genres.'
          });
          _context2.next = 14;
          break;
        case 6:
          _context2.next = 8;
          return _genresModel["default"].findOne(req.body);
        case 8:
          genres = _context2.sent;
          if (genres) {
            _context2.next = 13;
            break;
          }
          return _context2.abrupt("return", res.status(401).json({
            code: 401,
            msg: 'Authentication failed'
          }));
        case 13:
          return _context2.abrupt("return", res.status(200).json({
            code: 200,
            msg: "Authentication Successful",
            token: 'TEMPORARY_TOKEN'
          }));
        case 14:
        case "end":
          return _context2.stop();
      }
    }, _callee2);
  }));
  return function (_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}()));
router.put('/:id', /*#__PURE__*/function () {
  var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
    var result;
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) switch (_context3.prev = _context3.next) {
        case 0:
          if (req.body._id) delete req.body._id;
          _context3.next = 3;
          return _genresModel["default"].updateOne({
            _id: req.params.id
          }, req.body);
        case 3:
          result = _context3.sent;
          if (result.matchedCount) {
            res.status(200).json({
              code: 200,
              msg: 'Genre Updated Sucessfully'
            });
          } else {
            res.status(404).json({
              code: 404,
              msg: 'Unable to Update Genre'
            });
          }
        case 5:
        case "end":
          return _context3.stop();
      }
    }, _callee3);
  }));
  return function (_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
}());
router.get('/tmdb/movies', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref4 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
    var MovieGenres;
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return (0, _tmdbApi.getMovieGenres)();
        case 2:
          MovieGenres = _context4.sent;
          res.status(200).json(MovieGenres);
        case 4:
        case "end":
          return _context4.stop();
      }
    }, _callee4);
  }));
  return function (_x7, _x8) {
    return _ref4.apply(this, arguments);
  };
}()));
var _default = router;
exports["default"] = _default;
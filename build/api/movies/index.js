"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));
var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));
var _express = _interopRequireDefault(require("express"));
var _moviesData = require("./moviesData");
var _uniqid = _interopRequireDefault(require("uniqid"));
var _movieModel = _interopRequireDefault(require("./movieModel"));
var _expressAsyncHandler = _interopRequireDefault(require("express-async-handler"));
var _tmdbApi = require("../tmdb-api");
var router = _express["default"].Router();
router.get('/', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
    var movies;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return _movieModel["default"].find();
        case 2:
          movies = _context.sent;
          res.status(200).json(movies);
        case 4:
        case "end":
          return _context.stop();
      }
    }, _callee);
  }));
  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}()));
// Get movie details
router.get('/:id', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
    var id, movie;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) switch (_context2.prev = _context2.next) {
        case 0:
          id = parseInt(req.params.id);
          _context2.next = 3;
          return _movieModel["default"].findByMovieDBId(id);
        case 3:
          movie = _context2.sent;
          if (movie) {
            res.status(200).json(movie);
          } else {
            res.status(404).json({
              message: 'The resource you requested could not be found.',
              status_code: 404
            });
          }
        case 5:
        case "end":
          return _context2.stop();
      }
    }, _callee2);
  }));
  return function (_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}()));
// Get movie reviews
router.get('/:id/reviews', function (req, res) {
  var id = parseInt(req.params.id);
  // find reviews in list
  if (_moviesData.movieReviews.id == id) {
    res.status(200).json(_moviesData.movieReviews);
  } else {
    res.status(404).json({
      message: 'The resource you requested could not be found.',
      status_code: 404
    });
  }
});

//Post a movie review
router.post('/:id/reviews', function (req, res) {
  var id = parseInt(req.params.id);
  if (_moviesData.movieReviews.id == id) {
    req.body.created_at = new Date();
    req.body.updated_at = new Date();
    req.body.id = (0, _uniqid["default"])();
    _moviesData.movieReviews.results.push(req.body); //push the new review onto the list
    res.status(201).json(req.body);
  } else {
    res.status(404).json({
      message: 'The resource you requested could not be found.',
      status_code: 404
    });
  }
});
router.get('/tmdb/popular', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
    var Movies;
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return (0, _tmdbApi.getMovies)();
        case 2:
          Movies = _context3.sent;
          res.status(200).json(Movies);
        case 4:
        case "end":
          return _context3.stop();
      }
    }, _callee3);
  }));
  return function (_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
}()));
router.get('/tmdb/movies/:id', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref4 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
    var MovieDetail;
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return (0, _tmdbApi.getMovieDetail)(req.params.id);
        case 2:
          MovieDetail = _context4.sent;
          res.status(200).json(MovieDetail);
        case 4:
        case "end":
          return _context4.stop();
      }
    }, _callee4);
  }));
  return function (_x7, _x8) {
    return _ref4.apply(this, arguments);
  };
}()));
router.get('/tmdb/movies/:id/images', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref5 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req, res) {
    var MovieImages;
    return _regenerator["default"].wrap(function _callee5$(_context5) {
      while (1) switch (_context5.prev = _context5.next) {
        case 0:
          _context5.next = 2;
          return (0, _tmdbApi.getMovieImages)(req.params.id);
        case 2:
          MovieImages = _context5.sent;
          res.status(200).json(MovieImages);
        case 4:
        case "end":
          return _context5.stop();
      }
    }, _callee5);
  }));
  return function (_x9, _x10) {
    return _ref5.apply(this, arguments);
  };
}()));
router.get('/tmdb/movies/:id/reviews', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref6 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(req, res) {
    var MovieReviews;
    return _regenerator["default"].wrap(function _callee6$(_context6) {
      while (1) switch (_context6.prev = _context6.next) {
        case 0:
          _context6.next = 2;
          return (0, _tmdbApi.getMovieReviews)(req.params.id);
        case 2:
          MovieReviews = _context6.sent;
          res.status(200).json(MovieReviews);
        case 4:
        case "end":
          return _context6.stop();
      }
    }, _callee6);
  }));
  return function (_x11, _x12) {
    return _ref6.apply(this, arguments);
  };
}()));
router.get('/tmdb/upcoming', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref7 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee7(req, res) {
    var upcomingMovies;
    return _regenerator["default"].wrap(function _callee7$(_context7) {
      while (1) switch (_context7.prev = _context7.next) {
        case 0:
          _context7.next = 2;
          return (0, _tmdbApi.getUpcomingMovies)();
        case 2:
          upcomingMovies = _context7.sent;
          res.status(200).json(upcomingMovies);
        case 4:
        case "end":
          return _context7.stop();
      }
    }, _callee7);
  }));
  return function (_x13, _x14) {
    return _ref7.apply(this, arguments);
  };
}()));
router.get('/tmdb/toprated', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref8 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee8(req, res) {
    var topratedMovies;
    return _regenerator["default"].wrap(function _callee8$(_context8) {
      while (1) switch (_context8.prev = _context8.next) {
        case 0:
          _context8.next = 2;
          return (0, _tmdbApi.getTopratedMovies)();
        case 2:
          topratedMovies = _context8.sent;
          res.status(200).json(topratedMovies);
        case 4:
        case "end":
          return _context8.stop();
      }
    }, _callee8);
  }));
  return function (_x15, _x16) {
    return _ref8.apply(this, arguments);
  };
}()));
router.get('/tmdb/nowplaying', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref9 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee9(req, res) {
    var nowplayingMovies;
    return _regenerator["default"].wrap(function _callee9$(_context9) {
      while (1) switch (_context9.prev = _context9.next) {
        case 0:
          _context9.next = 2;
          return (0, _tmdbApi.getNowPlayingMovies)();
        case 2:
          nowplayingMovies = _context9.sent;
          res.status(200).json(nowplayingMovies);
        case 4:
        case "end":
          return _context9.stop();
      }
    }, _callee9);
  }));
  return function (_x17, _x18) {
    return _ref9.apply(this, arguments);
  };
}()));
var _default = router;
exports["default"] = _default;
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getUpcomingMovies = exports.getTopratedMovies = exports.getTVReviews = exports.getTVGenres = exports.getTVDetail = exports.getTV = exports.getSimilarTVshows = exports.getPopularPeople = exports.getPeopleimages = exports.getPeopleMovie_credit = exports.getPeopleDetail = exports.getNowPlayingMovies = exports.getMovies = exports.getMovieReviews = exports.getMovieImages = exports.getMovieGenres = exports.getMovieDetail = exports.getLatestPeople = exports.getLatestMovies = void 0;
var _nodeFetch = _interopRequireDefault(require("node-fetch"));
var getMovies = function getMovies() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/popular?api_key=".concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }
    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};
exports.getMovies = getMovies;
var getMovieDetail = function getMovieDetail(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/".concat(id, "?api_key=").concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }
    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};
exports.getMovieDetail = getMovieDetail;
var getUpcomingMovies = function getUpcomingMovies() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/upcoming?api_key=".concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }
    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};
exports.getUpcomingMovies = getUpcomingMovies;
var getTopratedMovies = function getTopratedMovies() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/top_rated?api_key=".concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }
    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};
exports.getTopratedMovies = getTopratedMovies;
var getNowPlayingMovies = function getNowPlayingMovies() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/now_playing?api_key=".concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }
    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};
exports.getNowPlayingMovies = getNowPlayingMovies;
var getLatestMovies = function getLatestMovies() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/latest?api_key=".concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }
    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};
exports.getLatestMovies = getLatestMovies;
var getPopularPeople = function getPopularPeople() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/person/popular?api_key=".concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }
    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};
exports.getPopularPeople = getPopularPeople;
var getPeopleDetail = function getPeopleDetail(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/person/".concat(id, "?api_key=").concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }
    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};
exports.getPeopleDetail = getPeopleDetail;
var getPeopleMovie_credit = function getPeopleMovie_credit(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/person/".concat(id, "/movie_credits?api_key=").concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }
    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};
exports.getPeopleMovie_credit = getPeopleMovie_credit;
var getLatestPeople = function getLatestPeople() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/person/latest?api_key=".concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }
    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};
exports.getLatestPeople = getLatestPeople;
var getPeopleimages = function getPeopleimages(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/person/".concat(id, "/images?api_key=").concat(process.env.REACT_APP_TMDB_KEY)).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }
    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};
exports.getPeopleimages = getPeopleimages;
var getTV = function getTV() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/tv/popular?api_key=".concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }
    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};
exports.getTV = getTV;
var getTVDetail = function getTVDetail(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/tv/".concat(id, "?api_key=").concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US&page=1&language=en-US")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }
    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};
exports.getTVDetail = getTVDetail;
var getSimilarTVshows = function getSimilarTVshows(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/tv/".concat(id, "/similar?api_key=").concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }
    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};
exports.getSimilarTVshows = getSimilarTVshows;
var getTVReviews = function getTVReviews(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/tv/".concat(id, "/reviews?api_key=").concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }
    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};
exports.getTVReviews = getTVReviews;
var getTVGenres = function getTVGenres(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/genre/tv/list?api_key=".concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }
    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};
exports.getTVGenres = getTVGenres;
var getMovieImages = function getMovieImages(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/".concat(id, "/images?api_key=").concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }
    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};
exports.getMovieImages = getMovieImages;
var getMovieGenres = function getMovieGenres() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/genre/movie/list?api_key=".concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }
    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};
exports.getMovieGenres = getMovieGenres;
var getMovieReviews = function getMovieReviews(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/".concat(id, "/reviews?api_key=").concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }
    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};
exports.getMovieReviews = getMovieReviews;
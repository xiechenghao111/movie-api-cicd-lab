"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loadMovies = loadMovies;
exports.loadPeople = loadPeople;
exports.loadTV = loadTV;
var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));
var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));
var _userModel = _interopRequireDefault(require("../api/users/userModel"));
var _genresModel = _interopRequireDefault(require("../api/genres/genresModel"));
var _users = _interopRequireDefault(require("./users"));
var _genres = _interopRequireDefault(require("./genres"));
var _dotenv = _interopRequireDefault(require("dotenv"));
var _movieModel = _interopRequireDefault(require("../api/movies/movieModel"));
var _movies = _interopRequireDefault(require("./movies.js"));
var _reviewModel = _interopRequireDefault(require("../api/reviews/reviewModel"));
var _review = _interopRequireDefault(require("./review.js"));
var _people = _interopRequireDefault(require("./people"));
var _peopleModel = _interopRequireDefault(require("../api/people/peopleModel"));
var _TVModel = _interopRequireDefault(require("../api/TV/TVModel"));
var _TV = _interopRequireDefault(require("./TV"));
_dotenv["default"].config();

// deletes all user documents in collection and inserts test data
function loadUsers() {
  return _loadUsers.apply(this, arguments);
}
function _loadUsers() {
  _loadUsers = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) switch (_context.prev = _context.next) {
        case 0:
          console.log('load user Data');
          _context.prev = 1;
          _context.next = 4;
          return _userModel["default"].deleteMany();
        case 4:
          _context.next = 6;
          return _users["default"].forEach(function (user) {
            return _userModel["default"].create(user);
          });
        case 6:
          console.info("".concat(_users["default"].length, " users were successfully stored."));
          _context.next = 12;
          break;
        case 9:
          _context.prev = 9;
          _context.t0 = _context["catch"](1);
          console.error("failed to Load user Data: ".concat(_context.t0));
        case 12:
        case "end":
          return _context.stop();
      }
    }, _callee, null, [[1, 9]]);
  }));
  return _loadUsers.apply(this, arguments);
}
function loadReviews() {
  return _loadReviews.apply(this, arguments);
}
function _loadReviews() {
  _loadReviews = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2() {
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) switch (_context2.prev = _context2.next) {
        case 0:
          console.log('load review Data');
          _context2.prev = 1;
          _context2.next = 4;
          return _reviewModel["default"].deleteMany();
        case 4:
          _context2.next = 6;
          return _reviewModel["default"].collection.insertMany(_review["default"]);
        case 6:
          console.info("".concat(_review["default"].length, " reviews were successfully stored."));
          _context2.next = 12;
          break;
        case 9:
          _context2.prev = 9;
          _context2.t0 = _context2["catch"](1);
          console.error("failed to Load review Data: ".concat(_context2.t0));
        case 12:
        case "end":
          return _context2.stop();
      }
    }, _callee2, null, [[1, 9]]);
  }));
  return _loadReviews.apply(this, arguments);
}
function loadGenres() {
  return _loadGenres.apply(this, arguments);
}
function _loadGenres() {
  _loadGenres = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3() {
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) switch (_context3.prev = _context3.next) {
        case 0:
          console.log('load genres Data');
          _context3.prev = 1;
          _context3.next = 4;
          return _genresModel["default"].deleteMany();
        case 4:
          _context3.next = 6;
          return _genresModel["default"].collection.insertMany(_genres["default"]);
        case 6:
          console.info("".concat(_genres["default"].length, " genres were successfully stored."));
          _context3.next = 12;
          break;
        case 9:
          _context3.prev = 9;
          _context3.t0 = _context3["catch"](1);
          console.error("failed to Load genres Data: ".concat(_context3.t0));
        case 12:
        case "end":
          return _context3.stop();
      }
    }, _callee3, null, [[1, 9]]);
  }));
  return _loadGenres.apply(this, arguments);
}
function loadMovies() {
  return _loadMovies.apply(this, arguments);
}
function _loadMovies() {
  _loadMovies = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4() {
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) switch (_context4.prev = _context4.next) {
        case 0:
          console.log('load seed data');
          console.log(_movies["default"].length);
          _context4.prev = 2;
          _context4.next = 5;
          return _movieModel["default"].deleteMany();
        case 5:
          _context4.next = 7;
          return _movieModel["default"].collection.insertMany(_movies["default"]);
        case 7:
          console.info("".concat(_movies["default"].length, " Movies were successfully stored."));
          _context4.next = 13;
          break;
        case 10:
          _context4.prev = 10;
          _context4.t0 = _context4["catch"](2);
          console.error("failed to Load movie Data: ".concat(_context4.t0));
        case 13:
        case "end":
          return _context4.stop();
      }
    }, _callee4, null, [[2, 10]]);
  }));
  return _loadMovies.apply(this, arguments);
}
function loadPeople() {
  return _loadPeople.apply(this, arguments);
}
function _loadPeople() {
  _loadPeople = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5() {
    return _regenerator["default"].wrap(function _callee5$(_context5) {
      while (1) switch (_context5.prev = _context5.next) {
        case 0:
          console.log('load seed data');
          console.log(_people["default"].length);
          _context5.prev = 2;
          _context5.next = 5;
          return _peopleModel["default"].deleteMany();
        case 5:
          _context5.next = 7;
          return _peopleModel["default"].collection.insertMany(_people["default"]);
        case 7:
          console.info("".concat(_people["default"].length, " people were successfully stored."));
          _context5.next = 13;
          break;
        case 10:
          _context5.prev = 10;
          _context5.t0 = _context5["catch"](2);
          console.error("failed to Load person Data: ".concat(_context5.t0));
        case 13:
        case "end":
          return _context5.stop();
      }
    }, _callee5, null, [[2, 10]]);
  }));
  return _loadPeople.apply(this, arguments);
}
function loadTV() {
  return _loadTV.apply(this, arguments);
}
function _loadTV() {
  _loadTV = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6() {
    return _regenerator["default"].wrap(function _callee6$(_context6) {
      while (1) switch (_context6.prev = _context6.next) {
        case 0:
          console.log('load seed data');
          console.log(_TV["default"].length);
          _context6.prev = 2;
          _context6.next = 5;
          return _TVModel["default"].deleteMany();
        case 5:
          _context6.next = 7;
          return _TVModel["default"].collection.insertMany(_TV["default"]);
        case 7:
          console.info("".concat(_TV["default"].length, " TV were successfully stored."));
          _context6.next = 13;
          break;
        case 10:
          _context6.prev = 10;
          _context6.t0 = _context6["catch"](2);
          console.error("failed to Load TV Data: ".concat(_context6.t0));
        case 13:
        case "end":
          return _context6.stop();
      }
    }, _callee6, null, [[2, 10]]);
  }));
  return _loadTV.apply(this, arguments);
}
if (process.env.SEED_DB) {
  loadUsers();
  loadGenres();
  loadMovies();
  loadReviews();
  loadPeople();
  loadTV();
}